<?php
  get_header();

  get_template_part('template-parts/hero');

  get_template_part('template-parts/featured');

  get_template_part('template-parts/posts');

  get_footer();
?>

