
  <footer class="footer" id="page-footer">
    <div class="footer__wrapper">
      <div class="footer__social social">
        <span class="footer__text">Share on</span>
        <a class="social__link" href="">
          <svg class="social__icon" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
            viewBox="0 0 9 16" style="enable-background:new 0 0 9 16;" xml:space="preserve">
          <title></title>
          <desc>Created with Sketch.</desc>
          <g id="BATCH-A">
            <g id="Splash-Page-Desktop-800" transform="translate(-570.000000, -571.000000)">
              <g id="Group-4" transform="translate(570.000000, 571.000000)">
                <path id="_xF09A_" d="M8.2,1.1v2.3H6.9c-0.5,0-0.8,0.1-1,0.3C5.7,3.9,5.6,4.2,5.6,4.6v1.6h2.5L7.8,8.7H5.6v6.5H3V8.7H0.8V6.2H3
                  V4.3c0-1.1,0.3-1.9,0.9-2.5S5.3,1,6.3,1C7.1,1,7.8,1,8.2,1.1z"/>
              </g>
            </g>
          </g>
          </svg>
        </a>
        <a class="social__link" href="">
          <svg class="social__icon" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
            viewBox="0 0 14 12" style="enable-background:new 0 0 14 12;" xml:space="preserve">
          <title></title>
          <desc>Created with Sketch.</desc>
          <g id="BATCH-A">
            <g id="Splash-Page-Desktop-800" transform="translate(-591.000000, -574.000000)">
              <g id="Group-4" transform="translate(570.000000, 571.000000)">
                <path id="_xF099_" d="M34.9,4.5c-0.4,0.6-0.8,1-1.4,1.4c0,0.1,0,0.2,0,0.4c0,0.7-0.1,1.5-0.3,2.2s-0.5,1.5-1,2.1
                  c-0.4,0.7-1,1.3-1.6,1.8c-0.6,0.5-1.4,0.9-2.2,1.3c-0.9,0.3-1.8,0.5-2.8,0.5c-1.6,0-3-0.4-4.3-1.2c0.2,0,0.4,0,0.7,0
                  c1.3,0,2.4-0.4,3.4-1.2c-0.6,0-1.1-0.2-1.6-0.6s-0.8-0.8-1-1.4c0.2,0,0.4,0,0.5,0c0.2,0,0.5,0,0.7-0.1c-0.6-0.1-1.2-0.5-1.6-1
                  c-0.4-0.5-0.6-1.1-0.6-1.8v0c0.4,0.2,0.8,0.3,1.3,0.4c-0.4-0.3-0.7-0.6-0.9-1c-0.2-0.4-0.3-0.8-0.3-1.3c0-0.5,0.1-1,0.4-1.4
                  c0.7,0.9,1.5,1.5,2.5,2c1,0.5,2.1,0.8,3.2,0.9C28,6.4,28,6.2,28,5.9c0-0.8,0.3-1.4,0.8-2c0.5-0.5,1.2-0.8,2-0.8
                  c0.8,0,1.5,0.3,2,0.9c0.6-0.1,1.2-0.3,1.8-0.7c-0.2,0.7-0.6,1.2-1.2,1.5C33.9,4.9,34.4,4.7,34.9,4.5z"/>
              </g>
            </g>
          </g>
          </svg>
        </a>
      </div>
      <div class="footer__logo">
        <img class="logo" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/www.knobcreek.com-1311011787501770.svg" alt="">
      </div>
    </div>
  </footer>

<?php wp_footer(); ?>
</body>
</html>