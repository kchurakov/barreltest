var gulp = require('gulp'),
    sass = require('gulp-sass'),
    watch = require('gulp-watch');
// var uglify = require('gulp-uglifycss');
// var autoprefixer = require('gulp-autoprefixer');
// var browserSync = require('browser-sync').create();

gulp.task('sass', function() {
    return gulp.src('src/scss/**/*.scss')
        .pipe(sass())
        .pipe(gulp.dest('assets/css'))

        // .pipe(uglify())
        // .pipe(gulp.dest('assets/css/minified'))
        // .pipe(browserSync.stream());
});

gulp.task('autoprefixer', function() {
    return gulp.src('assets/css/*.css')
        .pipe(autoprefixer())
        .pipe(uglify())
        .pipe(gulp.dest('assets/css/new'))
});

gulp.task('compile', ['sass'], function() {
    // browserSync.init({
    //     proxy: ''
    // });
    gulp.watch('src/scss/**/*.scss', ['sass']);
    // gulp.watch('assets/css/**/*.css', ['autoprefixer']);
    // gulp.watch(['src/scss/**/*.scss', '*.php']).on('change', browserSync.reload);
});