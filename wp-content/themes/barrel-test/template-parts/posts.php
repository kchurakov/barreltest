<section class="section section-articles" id="section-articles">
  <div class="section__wrapper">
    <div class="section__header">
      <h2 class="heading section__heading">Recent Articles</h2>
    </div>
    <div class="section__content articles">
      <!-- Articles loop START -->
      <?php $args = array('post_type' => 'post',  'posts_per_page' => 8, 'offset' => 1 ); ?>
      <?php $articles_loop = new WP_Query($args); ?>
      <?php if ( $articles_loop->have_posts() ) : while ( $articles_loop->have_posts() ) : $articles_loop->the_post(); ?>
        <!-- Articles item START -->
        <div class="articles__item preview">
          <div class="preview__wrapper">
            <div class="preview__image" style="background-image: url('<?php the_post_thumbnail_url() ?>')">
            </div>
            <div class="preview__header">
              <div class="preview__icon">
                <?php $category = get_the_category(); $article_category = $category[0]->slug; ?>
                <?php switch ($article_category) {
                  case 'video':
                    echo '<svg class="icon" version="1.1"	 id="Layer_1" xmlns:cc="http://creativecommons.org/ns#" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd" xmlns:svg="http://www.w3.org/2000/svg"	 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 22.3 22.3"	 style="enable-background:new 0 0 22.3 22.3;" xml:space="preserve"><title>Video Icon</title><g transform="translate(0,-952.36218)">	<path d="M11.9,972.8h3.6c0.5,0,0.9-0.4,0.9-0.9v-1l4.5,1c0.5,0.1,1.1-0.3,1.2-0.9l0-7.5c-0.1-0.7-0.7-1-1.2-0.9l-4.5,1l0-1		c0-0.5-0.4-0.9-0.9-0.9H15c0.9-0.8,1.5-2.1,1.5-3.4c0-2.6-2.1-4.7-4.7-4.7c-1.9,0-3.6,1.2-4.3,2.8c-0.7-1-1.9-1.6-3.2-1.6		c-2.2,0-4.1,1.8-4.1,4.1c0,1.1,0.4,2.1,1.2,2.8H1.2c-0.5,0-0.9,0.5-0.9,0.9l0,9.4c0,0.5,0.4,0.9,0.9,0.9h3.7h2.3H11.9z M11.8,955.3		c1.6,0,2.8,1.2,2.8,2.8c0,1.6-1.2,2.8-2.8,2.8S9,959.7,9,958.1C9,956.6,10.2,955.3,11.8,955.3z M4.3,956.6c1.2,0,2.2,1,2.2,2.2		c0,1.2-1,2.2-2.2,2.2s-2.2-1-2.2-2.2C2.1,957.6,3.1,956.6,4.3,956.6z M7.9,960.7c0.2,0.3,0.5,0.6,0.7,0.9H7.2		C7.5,961.3,7.7,961,7.9,960.7z M2.1,963.5h12.5l0,7.5H2.1L2.1,963.5z M20.2,964.6l0,5.2l-3.7-0.8l0-3.5L20.2,964.6z M7.2,972.8"/></g></svg>';
                    break;
                  case 'gallery':
                    echo '<svg class="icon" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 22.3 22.3" style="enable-background:new 0 0 22.3 22.3;" xml:space="preserve"><style type="text/css">	.st0{fill:#BD3131;}</style><title>Gallery Icon</title><desc>Created with Sketch.</desc><g id="Symbols">	<g>		<g id="GalleryICon_12_" transform="translate(-17.000000, -18.000000)">			<g id="Group-6_12_">				<g id="Group-5_12_" transform="translate(17.000000, 18.000000)">					<g id="Rectangle-4_55_">						<path class="st0" d="M20.4,10.2h-6.5c-1,0-1.8-0.8-1.8-1.8V2.9c0-1,0.8-1.8,1.8-1.8h6.5c1,0,1.8,0.8,1.8,1.8v5.5							C22.2,9.4,21.4,10.2,20.4,10.2z M13.9,2.7c-0.1,0-0.2,0.1-0.2,0.2v5.5c0,0.1,0.1,0.2,0.2,0.2h6.5c0.1,0,0.2-0.1,0.2-0.2V2.9							c0-0.1-0.1-0.2-0.2-0.2H13.9z"/>					</g>					<g id="Rectangle-4_54_">						<path class="st0" d="M8.4,10.2H1.9c-1,0-1.8-0.8-1.8-1.8V2.9c0-1,0.8-1.8,1.8-1.8h6.5c1,0,1.8,0.8,1.8,1.8v5.5							C10.2,9.4,9.4,10.2,8.4,10.2z M1.9,2.7c-0.1,0-0.2,0.1-0.2,0.2v5.5c0,0.1,0.1,0.2,0.2,0.2h6.5c0.1,0,0.2-0.1,0.2-0.2V2.9							c0-0.1-0.1-0.2-0.2-0.2H1.9z"/>					</g>					<g id="Rectangle-4_53_">						<path class="st0" d="M8.4,21.2H1.9c-1,0-1.8-0.8-1.8-1.8v-5.5c0-1,0.8-1.8,1.8-1.8h6.5c1,0,1.8,0.8,1.8,1.8v5.5							C10.2,20.4,9.4,21.2,8.4,21.2z M1.9,13.7c-0.1,0-0.2,0.1-0.2,0.2v5.5c0,0.1,0.1,0.2,0.2,0.2h6.5c0.1,0,0.2-0.1,0.2-0.2v-5.5							c0-0.1-0.1-0.2-0.2-0.2H1.9z"/>					</g>					<g id="Rectangle-4_52_">						<path class="st0" d="M20.4,21.2h-6.5c-1,0-1.8-0.8-1.8-1.8v-5.5c0-1,0.8-1.8,1.8-1.8h6.5c1,0,1.8,0.8,1.8,1.8v5.5							C22.2,20.4,21.4,21.2,20.4,21.2z M13.9,13.7c-0.1,0-0.2,0.1-0.2,0.2v5.5c0,0.1,0.1,0.2,0.2,0.2h6.5c0.1,0,0.2-0.1,0.2-0.2							v-5.5c0-0.1-0.1-0.2-0.2-0.2H13.9z"/>					</g>				</g>			</g>		</g>	</g></g></svg>';
                    break;
                  case 'glass':
                    echo '<svg class="icon" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"	 viewBox="0 0 22.3 22.3" style="enable-background:new 0 0 22.3 22.3;" xml:space="preserve"><title>Glass Icon</title><path d="M21.1,0.9c-0.1-0.1-0.3-0.2-0.4-0.2h-19c-0.2,0-0.3,0.1-0.4,0.2S1.1,1.2,1.1,1.4l1.6,17.9c0.1,1,0.9,1.8,2,1.8h13.1	c1,0,1.9-0.8,2-1.8l1.6-17.9C21.3,1.2,21.2,1,21.1,0.9z M3.1,10.2h1v2.2c0,1.1,0.9,2,2,2h3.8c1,0,1.8-0.6,2-1.5l0.7,0.3	c0.2,0.1,0.5,0.1,0.7,0.1c0.3,0,0.6-0.1,0.9-0.2c0.5-0.2,0.9-0.6,1.1-1.2l0.6-1.8h1.1l-0.2,3c0,0.3,0.2,0.6,0.5,0.6h0	c0.3,0,0.6-0.2,0.6-0.5l0.3-3.2h1.2l-0.4,5.1H3.6L3.1,10.2z M15.4,8L15,9h-3.1V8.6c0-1.1-0.8-2-1.9-2l0.2-0.5	c0.1-0.2,0.2-0.4,0.4-0.5c0.2-0.1,0.4-0.1,0.6,0l3.6,1.3c0.2,0.1,0.4,0.2,0.5,0.4C15.4,7.5,15.4,7.7,15.4,8z M11.9,10.2h2.6	l-0.5,1.4c-0.1,0.2-0.2,0.4-0.4,0.5c-0.2,0.1-0.4,0.1-0.6,0L12,11.7L11.9,10.2L11.9,10.2z M9.9,7.8c0.5,0,0.8,0.4,0.8,0.8V9H5.2V8.6	c0-0.5,0.4-0.8,0.8-0.8H9.9z M10.8,10.2v2.2c0,0.5-0.4,0.8-0.8,0.8H6.1c-0.5,0-0.8-0.4-0.8-0.8v-2.2H10.8z M20,1.9L19.4,9h-1.3	l0.4-4.4c0-0.3-0.2-0.6-0.5-0.6c-0.3,0-0.6,0.2-0.6,0.5L17,9h-0.7l0.2-0.6c0.2-0.5,0.2-1.1-0.1-1.6c-0.2-0.5-0.6-0.9-1.2-1.1	l-3.6-1.3c-0.5-0.2-1.1-0.2-1.6,0.1C9.6,4.8,9.2,5.2,9.1,5.7L8.7,6.5H6.1c-1.1,0-2,0.9-2,2v0.4H3l-0.6-7H20z M17.7,19.9h-13	c-0.4,0-0.7-0.3-0.8-0.7l-0.2-2.7h15.1l-0.2,2.7C18.5,19.6,18.1,19.9,17.7,19.9z"/></svg>';
                    break;
                  default:
                    echo '<svg class="icon" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 22.3 22.3" style="enable-background:new 0 0 22.3 22.3;" xml:space="preserve"><title>Article Icon</title><path d="M19.6,3.9h-1.8V3c0-1.2-1-2.3-2.3-2.3H3.3C2.1,0.7,1,1.7,1,3v15.7c0,1.6,1.3,3,3,3h14.9c1.4,0,2.5-1.1,2.5-2.5V5.7	C21.4,4.7,20.6,3.9,19.6,3.9z M4,20.3c-0.9,0-1.6-0.7-1.6-1.6V3c0-0.5,0.4-0.9,0.9-0.9h12.2c0.5,0,0.9,0.4,0.9,0.9v16.1	c0,0.4,0.1,0.8,0.3,1.1H4z M20,19.1c0,0.6-0.5,1.1-1.1,1.1s-1.1-0.5-1.1-1.1V5.3h1.8c0.2,0,0.5,0.2,0.5,0.5V19.1z"/><path d="M4.2,11.1h5.7c0.4,0,0.7-0.3,0.7-0.7V4.6c0-0.4-0.3-0.7-0.7-0.7H4.2C3.8,4,3.5,4.3,3.5,4.6v5.7C3.5,10.8,3.9,11.1,4.2,11.1z	 M4.9,5.3h4.3v4.4H4.9V5.3z"/><path d="M14.6,15.9H4.4c-0.4,0-0.7,0.3-0.7,0.7s0.3,0.7,0.7,0.7h10.2c0.4,0,0.7-0.3,0.7-0.7S15,15.9,14.6,15.9z"/><path d="M14.6,12.9H4.4c-0.4,0-0.7,0.3-0.7,0.7c0,0.4,0.3,0.7,0.7,0.7h10.2c0.4,0,0.7-0.3,0.7-0.7C15.3,13.2,15,12.9,14.6,12.9z"/><path d="M14.6,6.8H12c-0.4,0-0.7,0.3-0.7,0.7c0,0.4,0.3,0.7,0.7,0.7h2.7c0.4,0,0.7-0.3,0.7-0.7C15.3,7.1,15,6.8,14.6,6.8z"/><path d="M14.6,9.8H12c-0.4,0-0.7,0.3-0.7,0.7c0,0.4,0.3,0.7,0.7,0.7h2.7c0.4,0,0.7-0.3,0.7-0.7C15.3,10.1,15,9.8,14.6,9.8z"/><path d="M14.6,4.1H12c-0.4,0-0.7,0.3-0.7,0.7c0,0.4,0.3,0.7,0.7,0.7h2.7c0.4,0,0.7-0.3,0.7-0.7C15.3,4.4,15,4.1,14.6,4.1z"/></svg>';
                } ?>
              </div>
              <div class="preview__date">
                <?php echo get_the_date('F d'); ?>
              </div>
              <div class="preview__title heading heading--h4">
                <?php echo wp_trim_words(get_the_title($post), 5, '...'); ?>
              </div>
              <div class="preview__link">
                <a class="link" href="<?php the_permalink(); ?>">
                <?php $category = get_the_category(); $article_category = $category[0]->slug; ?>
                <?php switch ($article_category) {
                  case 'video':
                    echo 'Watch Video';
                    break;
                  case 'gallery':
                    echo 'View Gallery';
                    break;
                  default:
                    echo 'Read More';
                } ?>
                </a>
              </div>
            </div>
          </div>
        </div>
        <!-- Articles item END -->
      <?php endwhile; ?>
      <?php else: ?>
          <h3>No articles found!</h3>
      <?php endif; ?>
      <?php wp_reset_postdata(); ?>
      <!-- Articles loop END -->
    </div>
  </div>
</section>