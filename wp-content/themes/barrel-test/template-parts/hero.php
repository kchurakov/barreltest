
<section class="section section-hero" id="section-hero" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/assets/img/www.knobcreek.com-1293609732325191.jpg')">
  <div class="shadowed">
    <div class="section__wrapper hero">
      <div class="hero__heading">
        <h1 class="heading heading--h1">Maple never tasted so good</h1>
        <p class="hero__text">We blend this bourbon Lorem, ipsum dolor sit amet consectetur adipisicing elit!</p>
      </div>
      <div class="hero__image">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/www.knobcreek.com-1310894113736742.png" alt="">
      </div>
    </div>
  </div>
</section>