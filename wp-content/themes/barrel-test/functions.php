<?php

function extend_theme_functions() {
    add_theme_support( 'post-thumbnails' );
    add_theme_support( 'post-formats', array(
        'image',
        'gallery',
        'video',
    ));
    add_theme_support( 'custom-logo' );
    add_theme_support( 'html5', array(
      'search-form'
    ));
}
add_action( 'init', 'extend_theme_functions', 0 );

function cc_mime_types($mimes) {
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

function enqueue_theme_styles() {
    wp_enqueue_style('theme-styles', get_stylesheet_directory_uri().'/assets/css/index.css');
}

add_action('wp_enqueue_scripts', 'enqueue_theme_styles');